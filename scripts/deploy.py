from brownie import Factory, SuperpositionSwap, SuperpositionSwapMeta, accounts

WETH = "0x"
DEPLOYER = accounts.load("test")
txparams = {"from", DEPLOYER}


def main():
    factory = Factory.deploy(txparams)
    base = SuperpositionSwap.deploy(factory, WETH, txparams)
    meta = SuperpositionSwapMeta.deploy(factory, WETH, txparams)
    factory.set_implementation(base, meta)
    print(f"Factory: {factory.address}\n")
