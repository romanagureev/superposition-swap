import pytest


@pytest.fixture(scope="session")
def weth(WETH, admin):
    return WETH.deploy({"from": admin})


@pytest.fixture(scope="session")
def btc(ERC20, admin):
    return ERC20.deploy("Bitcoin", "BTC", {"from": admin})


@pytest.fixture(scope="session")
def usd(ERC20, admin):
    return ERC20.deploy("US Dollar", "USD", {"from": admin})


@pytest.fixture(scope="session")
def coins(weth, btc):
    return [weth, btc]


@pytest.fixture(scope="session")
def initial_amounts(weth, btc):
    return [10 ** 6 * 10 ** coin.decimals() for coin in [weth, btc]]


@pytest.fixture(scope="session", autouse=True)
def mint_initial(weth, btc, usd, admin, alice):
    for coin in [weth, btc, usd]:
        coin._mint_for_testing(admin, 10 ** 8 * 10 ** coin.decimals(), {"from": admin})
        coin._mint_for_testing(alice, 10 ** 8 * 10 ** coin.decimals(), {"from": alice})
