import pytest

pytest_plugins = [
    "fixtures.accounts",
    "fixtures.coins",
]


@pytest.fixture(scope="session")
def factory(Factory, SuperpositionSwap, SuperpositionSwapMeta, weth, admin):
    contract = Factory.deploy(weth, {"from": admin})
    base = SuperpositionSwap.deploy(factory, weth, {"from": admin})
    meta = SuperpositionSwapMeta.deploy(factory, weth, {"from": admin})
    contract.set_implementation(base, meta, {"from": admin})


@pytest.fixture(scope="module")
def swap(factory, SuperpositionSwap, coins, admin):
    addr = factory.deploy_base(
        f"{coins[0].name()} and {coins[1].name()}",
        f"{coins[0].symbol()}/{coins[1].symbol()}",
        coins,
        {"from": admin},
    )
    return SuperpositionSwap.at(addr)


@pytest.fixture(scope="module")
def provide_initial_liquidity(swap, initial_amounts, admin):
    swap.add_liquidity(initial_amounts, [0, 0], {"from": admin})


@pytest.fixture(scope="session", autouse=True)
def approve_swap(swap, coins, admin, alice):
    for coin in coins:
        coin.approve(swap, 2 ** 256 - 1, {"from": admin})
        coin.approve(swap, 2 ** 256 - 1, {"from": alice})
