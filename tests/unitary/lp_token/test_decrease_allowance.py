import brownie


def test_decrease_allowance_to_zero(token, alice, bob):
    token.approve(bob, 10 ** 19, {"from": alice})
    token.decreaseAllowance(bob, 10 ** 19, {"from": alice})

    assert token.allowance(alice, bob) == 0


def test_decrease_allowance(token, alice, bob):
    token.approve(bob, 10 ** 19, {"from": alice})
    token.decreaseAllowance(bob, 123456789, {"from": alice})

    assert token.allowance(alice, bob) == 10 ** 19 - 123456789


def test_revoke_decrease_allowance(token, alice, bob):
    token.approve(bob, 10 ** 19, {"from": alice})
    token.increaseAllowance(bob, 123456789, {"from": alice})
    token.approve(bob, 0, {"from": alice})

    assert token.allowance(alice, bob) == 0


def test_decrease_allowance_self(token, alice):
    token.approve(alice, 10 ** 19, {"from": alice})
    token.decreaseAllowance(alice, 123456789, {"from": alice})

    assert token.allowance(alice, alice) == 10 ** 19 - 123456789


def test_only_affects_target(token, alice, bob):
    token.approve(bob, 10 ** 19, {"from": alice})
    token.decreaseAllowance(bob, 123456789, {"from": alice})

    assert token.allowance(bob, alice) == 0


def test_returns_true(token, alice, bob):
    token.approve(bob, 10 ** 19, {"from": alice})
    tx = token.decreaseAllowance(bob, 10 ** 19, {"from": alice})

    assert tx.return_value is True


def test_decrease_too_much(token, alice, bob):
    token.approve(alice, 10 ** 19, {"from": alice})
    with brownie.reverts():
        token.decreaseAllowance(bob, 10 ** 19 + 1, {"from": alice})


def test_approval_event_fires(token, alice, bob):
    token.approve(bob, 10 ** 19, {"from": alice})
    tx = token.decreaseAllowance(bob, 123456789, {"from": alice})

    assert len(tx.events) == 1
    event = tx.events["Approval"]
    assert event["_owner"] == alice
    assert event["_spender"] == bob
    assert event["_value"] == 10 ** 19 - 123456789
    assert event.values() == [alice, bob, 10 ** 19 - 123456789]
