import brownie


def test_setup(token, alice):
    assert token.balanceOf(alice) >= 4


def test_sender_balance_decreases(token, alice, bob):
    sender_balance = token.balanceOf(alice)
    amount = sender_balance // 4

    token.transfer(bob, amount, {"from": alice})

    assert token.balanceOf(alice) == sender_balance - amount


def test_receiver_balance_increases(token, alice, bob):
    receiver_balance = token.balanceOf(bob)
    amount = token.balanceOf(alice) // 4

    token.transfer(bob, amount, {"from": alice})

    assert token.balanceOf(bob) == receiver_balance + amount


def test_total_supply_not_affected(token, alice, bob):
    total_supply = token.totalSupply()
    amount = token.balanceOf(alice)

    token.transfer(bob, amount, {"from": alice})

    assert token.totalSupply() == total_supply


def test_returns_true(token, alice, bob):
    amount = token.balanceOf(alice)
    tx = token.transfer(bob, amount, {"from": alice})

    assert tx.return_value is True


def test_transfer_full_balance(token, alice, bob):
    amount = token.balanceOf(alice)
    receiver_balance = token.balanceOf(bob)

    token.transfer(bob, amount, {"from": alice})

    assert token.balanceOf(alice) == 0
    assert token.balanceOf(bob) == receiver_balance + amount


def test_transfer_zero_tokens(token, alice, bob):
    sender_balance = token.balanceOf(alice)
    receiver_balance = token.balanceOf(bob)

    token.transfer(bob, 0, {"from": alice})

    assert token.balanceOf(alice) == sender_balance
    assert token.balanceOf(bob) == receiver_balance


def test_transfer_to_self(token, alice):
    sender_balance = token.balanceOf(alice)
    amount = sender_balance // 4

    token.transfer(alice, amount, {"from": alice})

    assert token.balanceOf(alice) == sender_balance


def test_insufficient_balance(token, alice, bob):
    balance = token.balanceOf(alice)

    with brownie.reverts():
        token.transfer(bob, balance + 1, {"from": alice})


def test_transfer_event_fires(token, alice, bob):
    amount = token.balanceOf(alice)
    tx = token.transfer(bob, amount, {"from": alice})

    assert len(tx.events) == 1
    event = tx.events["Transfer"]
    assert event["_from"] == alice
    assert event["_to"] == bob
    assert event["_value"] == amount
    assert event.values() == [alice, bob, amount]
