import brownie


def test_sender_balance_decreases(token, alice, bob, charlie):
    sender_balance = token.balanceOf(alice)
    amount = sender_balance // 4

    token.approve(charlie, amount, {"from": alice})
    token.transferFrom(alice, bob, amount, {"from": charlie})

    assert token.balanceOf(alice) == sender_balance - amount


def test_receiver_balance_increases(token, alice, bob, charlie):
    receiver_balance = token.balanceOf(bob)
    amount = token.balanceOf(alice) // 4

    token.approve(charlie, amount, {"from": alice})
    token.transferFrom(alice, bob, amount, {"from": charlie})

    assert token.balanceOf(bob) == receiver_balance + amount


def test_caller_balance_not_affected(token, alice, bob, charlie):
    caller_balance = token.balanceOf(bob)
    amount = token.balanceOf(alice)

    token.approve(charlie, amount, {"from": alice})
    token.transferFrom(alice, bob, amount, {"from": charlie})

    assert token.balanceOf(charlie) == caller_balance


def test_caller_approval_affected(token, alice, bob, charlie):
    approval_amount = token.balanceOf(alice)
    transfer_amount = approval_amount // 4

    token.approve(charlie, approval_amount, {"from": alice})
    token.transferFrom(alice, bob, transfer_amount, {"from": charlie})

    assert token.allowance(alice, charlie) == approval_amount - transfer_amount


def test_receiver_approval_not_affected(token, alice, bob, charlie):
    approval_amount = token.balanceOf(alice)
    transfer_amount = approval_amount // 4

    token.approve(bob, approval_amount, {"from": alice})
    token.approve(charlie, approval_amount, {"from": alice})
    token.transferFrom(alice, bob, transfer_amount, {"from": charlie})

    assert token.allowance(alice, bob) == approval_amount


def test_total_supply_not_affected(token, alice, bob, charlie):
    total_supply = token.totalSupply()
    amount = token.balanceOf(alice)

    token.approve(charlie, amount, {"from": alice})
    token.transferFrom(alice, bob, amount, {"from": charlie})

    assert token.totalSupply() == total_supply


def test_returns_true(token, alice, bob, charlie):
    amount = token.balanceOf(alice)
    token.approve(charlie, amount, {"from": alice})
    tx = token.transferFrom(alice, bob, amount, {"from": charlie})

    assert tx.return_value is True


def test_transfer_full_balance(token, alice, bob, charlie):
    amount = token.balanceOf(alice)
    receiver_balance = token.balanceOf(bob)

    token.approve(charlie, amount, {"from": alice})
    token.transferFrom(alice, bob, amount, {"from": charlie})

    assert token.balanceOf(alice) == 0
    assert token.balanceOf(bob) == receiver_balance + amount


def test_transfer_zero_tokens(token, alice, bob, charlie):
    sender_balance = token.balanceOf(alice)
    receiver_balance = token.balanceOf(bob)

    token.approve(charlie, sender_balance, {"from": alice})
    token.transferFrom(alice, bob, 0, {"from": charlie})

    assert token.balanceOf(alice) == sender_balance
    assert token.balanceOf(bob) == receiver_balance


def test_transfer_zero_tokens_without_approval(token, alice, bob, charlie):
    sender_balance = token.balanceOf(alice)
    receiver_balance = token.balanceOf(bob)

    token.transferFrom(alice, bob, 0, {"from": charlie})

    assert token.balanceOf(alice) == sender_balance
    assert token.balanceOf(bob) == receiver_balance


def test_insufficient_balance(token, alice, bob, charlie):
    balance = token.balanceOf(alice)

    token.approve(charlie, balance + 1, {"from": alice})
    with brownie.reverts():
        token.transferFrom(alice, bob, balance + 1, {"from": charlie})


def test_insufficient_approval(token, alice, bob, charlie):
    balance = token.balanceOf(alice)

    token.approve(charlie, balance - 1, {"from": alice})
    with brownie.reverts():
        token.transferFrom(alice, bob, balance, {"from": charlie})


def test_no_approval(token, alice, bob, charlie):
    balance = token.balanceOf(alice)

    with brownie.reverts():
        token.transferFrom(alice, bob, balance, {"from": charlie})


def test_revoked_approval(token, alice, bob, charlie):
    balance = token.balanceOf(alice)

    token.approve(charlie, balance, {"from": alice})
    token.approve(charlie, 0, {"from": alice})

    with brownie.reverts():
        token.transferFrom(alice, bob, balance, {"from": charlie})


def test_transfer_to_self(token, alice):
    sender_balance = token.balanceOf(alice)
    amount = sender_balance // 4

    token.approve(alice, sender_balance, {"from": alice})
    token.transferFrom(alice, alice, amount, {"from": alice})

    assert token.balanceOf(alice) == sender_balance
    assert token.allowance(alice, alice) == sender_balance - amount


def test_transfer_to_self_no_approval(token, alice):
    amount = token.balanceOf(alice)

    with brownie.reverts():
        token.transferFrom(alice, alice, amount, {"from": alice})


def test_transfer_event_fires(token, alice, bob, charlie):
    amount = token.balanceOf(alice)

    token.approve(charlie, amount, {"from": alice})
    tx = token.transferFrom(alice, bob, amount, {"from": charlie})

    assert len(tx.events) == 1
    event = tx.events["Transfer"]
    assert event["_from"] == alice
    assert event["_to"] == bob
    assert event["_value"] == amount
    assert event.values() == [alice, bob, amount]
