from brownie.test import strategy


class Base:
    exchange_amount_in = strategy("uint256", max_value=10 ** 9 * 10 ** 18)
    exchange_i = strategy("uint8", max_value=1)

    def __init__(self, alice, bob, swap, coins):
        self.alice = alice
        self.bob = bob
        self.swap = swap
        self.coins = coins

    def rule_add_liquidity(self):
        pass

    def rule_exchange(self, exchange_amount_in, exchange_i):
        min_dy = (
            self.swap.calc_exchange(exchange_i, 1 - exchange_i, exchange_amount_in)
            * 100
            // 99
        )
        self.swap.exchange(exchange_i, 1 - exchange_i, exchange_amount_in, min_dy)

    def rule_remove_liquidity(self):
        pass
