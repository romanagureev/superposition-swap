import math

import pytest

N_COINS = 2
PRECISION = 10 ** 18


class SuperpositionSwap:
    def __init__(self, w: int):
        """
        @param w: Maximum weight of a token
        """
        self.x: list = [[0] * N_COINS for _ in range(2)]
        self.w: int = w
        self.fee: int = 0

        self.total_supply: list = [0] * 2

    def _level(self, k: int) -> int:
        level = PRECISION
        for i in range(N_COINS):
            level *= int(
                (self.x[k][i] / PRECISION) ** (self._norm_weight(k, i) / PRECISION)
                * PRECISION
            )
            level //= PRECISION
        return level

    def add_liquidity(self, amounts: tuple, proportions: tuple = None) -> (int, int):
        """
        Provide liquidity to the swap
        :param amounts: Amounts of tokens to add
        :param proportions:
        :return: Amounts of LP Tokens received
        """
        assert len(amounts) == N_COINS, "Incorrect amount of coins"

        # Initial supply
        if self.total_supply[0] == 0 or self.total_supply[1] == 0:
            if not proportions:
                # Make 50/50 for the first deposit
                self.x[1][0] = amounts[0] // (1 + self.w)
                self.x[0][0] = amounts[0] - self.x[1][0]

                self.x[0][1] = amounts[1] // (1 + self.w)
                self.x[1][1] = amounts[1] - self.x[0][1]

                self.total_supply[0] = self._level(0)
                self.total_supply[1] = self._level(1)

                self.spot_price(0, 1)  # checks that prices are equal
                return self.total_supply[0], self.total_supply[1]
            else:
                assert min(proportions) * (1 + self.w) > PRECISION, "Bad proportions"
                # price = int(math.sqrt(amounts[0] * proportions[1] * PRECISION ** 2 // (amounts[1] * proportions[0])))
                # self.x[0][1] = (self.w * amounts[0] * price // PRECISION - amounts[1]) // (self.w ** 2 - 1)
                # self.x[1][1] = amounts[1] - self.x[0][1]
                #
                # self.x[0][0] = self.x[0][1] * self.w * price // PRECISION
                # self.x[1][0] = amounts[0] - self.x[0][0]
                self.x[0][0] = (
                    amounts[0]
                    * self.w
                    * (self.w * proportions[0] - proportions[1])
                    // (proportions[0] * (self.w ** 2 - 1))
                )
                self.x[0][1] = (
                    amounts[1]
                    * (self.w * proportions[0] - proportions[1])
                    // (proportions[1] * (self.w ** 2 - 1))
                )

                self.x[1][0] = amounts[0] - self.x[0][0]
                self.x[1][1] = amounts[1] - self.x[0][1]

                self.total_supply[0] = self._level(0)
                self.total_supply[1] = self._level(1)

                self.spot_price(0, 1)  # checks that prices are equal
                return self.total_supply[0], self.total_supply[1]
        if not proportions:
            initial_spot_price = self.spot_price(0, 1)
            a0 = (
                amounts[1] * self.x[0][0] * self.x[1][0]
                - amounts[0] * self.x[0][0] * self.x[1][1]
            ) // (self.x[0][1] * self.x[1][0] - self.x[0][0] * self.x[1][1])
            a1 = a0 * self.x[0][1] // self.x[0][0]

            initial_levels = tuple(self._level(i) for i in range(N_COINS))

            self.x[0][0] += a0
            self.x[0][1] += a1
            self.x[1][0] += amounts[0] - a0
            self.x[1][1] += amounts[1] - a1

            new_levels = tuple(self._level(i) for i in range(N_COINS))
            to_mint = tuple(
                self.total_supply[i]
                * (new_levels[i] - initial_levels[i])
                // initial_levels[i]
                for i in range(N_COINS)
            )
            self.total_supply[0] += to_mint[0]
            self.total_supply[1] += to_mint[1]

            assert self.spot_price(0, 1) == pytest.approx(initial_spot_price)
            return to_mint
        raise NotImplementedError

    def remove_liquidity(self, amounts: list, proportions: list) -> list:
        assert len(amounts) == 2, f"Incorrect len of LP amounts"
        to_return = [0, 0]
        for i in range(N_COINS):
            to_burn = amounts[i]
            dx = self.x[i][0] * to_burn // self.total_supply[i]
            dy = self.x[i][1] * to_burn // self.total_supply[i]

            self.x[i][0] -= dx
            self.x[i][1] -= dy

            to_return[0] += dx
            to_return[1] += dy

            self.total_supply[i] -= to_burn
        return to_return

    def _calc_exchange(self, i: int, j: int, dx: int) -> (int, int, int, int):
        """
        :param i: Token from
        :param j: Token to
        :param dx: Amount to swap
        :return: (dx0, dx1, dy0, dy1)
        """
        assert i != j, "Equal tokens"

        def calc_f(k: int, dx: int):
            f = (
                (self.x[k][i] + dx)
                * self._weight(k, j)
                * PRECISION
                // (self.x[k][j] * self._weight(k, i))
            )
            ratio = (self.x[k][i] * PRECISION + dx * (PRECISION - self.fee)) // self.x[
                k
            ][i]
            w_ratio = self._weight(k, i) / self._weight(k, j)
            f *= int((ratio / PRECISION) ** w_ratio * PRECISION)
            return f

        f0 = calc_f(0, 0)
        f1 = calc_f(1, dx)
        left = 0 if f0 < f1 else dx
        right = dx - left
        while abs(right - left) > 1:
            mid = (left + right) // 2
            f0 = calc_f(0, mid)
            f1 = calc_f(1, dx - mid)
            if f0 < f1:
                left = mid
            else:
                right = mid
        dx0 = left
        dx1 = dx - dx0

        def calc_dy(k: int, dx: int):
            ratio = (
                self.x[k][i]
                * PRECISION
                // (self.x[k][i] + dx * (PRECISION - self.fee) // PRECISION)
            )
            w_ratio = self._weight(k, i) / self._weight(k, j)
            return (
                self.x[k][j]
                * (PRECISION - int((ratio / PRECISION) ** w_ratio * PRECISION))
                // PRECISION
            )

        dy0 = calc_dy(0, dx0)
        dy1 = calc_dy(1, dx1)
        return dx0, dx1, dy0, dy1

    def calc_exchange(self, i: int, j: int, dx: int) -> int:
        (*_, dy0, dy1) = self._calc_exchange(i, j, dx)
        return dy0 + dy1

    def exchange(self, i: int, j: int, dx: int, min_dy: int = 0) -> int:
        dx0, dx1, dy0, dy1 = self._calc_exchange(i, j, dx)
        assert dy0 + dy1 >= min_dy, "Slippage"
        initial_levels = [self._level(0), self._level(1)]
        self.x[0][i] += dx0
        self.x[0][j] -= dy0
        self.x[1][i] += dx1
        self.x[1][j] -= dy1

        self.spot_price(i, j)  # checks that prices are equal
        new_levels = [self._level(0), self._level(1)]
        for i in range(2):
            if self.fee == 0:
                assert new_levels[i] == pytest.approx(
                    initial_levels[i]
                ), f"{new_levels[i]} != {initial_levels[i]}"
            else:
                assert (
                    new_levels[i] >= initial_levels[i]
                ), f"{new_levels[i]} < {initial_levels[i]}"
            for j in range(2):
                assert self.x[i][j] > 0
        return dy0 + dy1

    def _weight(self, k: int, i: int) -> int:
        """
        :param k: Index of the pool
        :param i: Index of the coin
        :return: Weight of coin in the pool
        """
        return self.w if k == i else 1

    def _norm_weight(self, k: int, i: int) -> int:
        """
        :param k: Index of the pool
        :param i: Index of the coin
        :return: Weight of coin in the pool with PRECISION
        """
        return self._weight(k, i) * PRECISION // (self.w + 1)

    def _spot_price(self, i: int, j: int, k: int) -> int:
        """
        :param i: Token price from
        :param j: Token price to
        :param k: Pool index
        :return: price
        """
        return (
            self.x[k][i]
            * self._weight(k, j)
            * PRECISION
            // (self.x[k][j] * self._weight(k, i))
        )

    def spot_price(self, i: int, j: int) -> int:
        """
        Current spot price according to internal invariant
        :param i: Token price from
        :param j: Token price to
        :return:
        """
        sp0 = self._spot_price(i, j, 0)
        sp1 = self._spot_price(i, j, 1)
        assert sp0 == pytest.approx(sp1), f"{sp0} != {sp1}"
        return (sp0 + sp1) // 2

    def _balance_in(self, i: int, k: int) -> int:
        return self.x[k][i] * PRECISION // self._norm_weight(k, i)

    def full_balance_in(self, i: int) -> int:
        balance0 = self._balance_in(i, 0)
        balance1 = self._balance_in(i, 1)
        return balance0 + balance1
