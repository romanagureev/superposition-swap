import pytest

PRECISION = 10 ** 18


class Balancer:
    def __init__(self, n: int, weights: tuple):
        """
        @param n: Number of coins in the pool
        @param weights: Weights of coins in the pool
        """
        assert len(weights) == n, "Incorrect weights length"
        self.N_COINS: int = n
        self.xs: list = [0] * n
        self.vxs: list = [0] * n
        self.a: list = [2 * PRECISION - w for w in weights]
        self.ws: tuple = weights
        assert sum(weights) == PRECISION, "Bad weights"
        self.fee: int = 0

        self.total_supply: int = 0

    @staticmethod
    def _level(xs, ws):
        level = PRECISION
        for x, w in zip(xs, ws):
            level *= int((x / PRECISION) ** (w / PRECISION) * PRECISION)
            level /= PRECISION
        return level

    def add_liquidity(self, amounts: tuple) -> int:
        assert len(amounts) == self.N_COINS, "Incorrect amount of coins"
        if self.xs[0] == 0:  # initial add_liquidity
            self.xs = list(amounts)
            self.vxs = [a * x // PRECISION for a, x in zip(self.a, amounts)]
            self.total_supply = self._level(self.vxs, self.ws)
            return self.total_supply

        initial_level = self._level(self.vxs, self.ws)
        self.xs = [self.xs[i] + amounts[i] for i in range(self.N_COINS)]
        self.vxs = [a * x // PRECISION for a, x in zip(self.a, self.xs)]

        new_level = self._level(self.vxs, self.ws)
        to_mint = self.total_supply * new_level // initial_level - self.total_supply
        self.total_supply += to_mint

        return to_mint

    def remove_liquidity(self, amount) -> list:
        share = amount * PRECISION // self.total_supply
        to_give = [self.xs[i] * share // PRECISION for i in range(self.N_COINS)]
        for i in range(self.N_COINS):
            self.xs[i] -= to_give[i]
            assert self.xs[i] >= 0
        self.vxs = [a * x // PRECISION for a, x in zip(self.a, self.xs)]
        return to_give

    def calc_exchange(self, i: int, j: int, dx: int) -> int:
        assert i != j
        dx = dx * (PRECISION - self.fee) // PRECISION
        balance_ratio = self.vxs[i] * PRECISION // (self.vxs[i] + dx)
        dy = (
            self.vxs[j]
            * (
                PRECISION
                - int(
                    (balance_ratio / PRECISION) ** (self.ws[i] / self.ws[j]) * PRECISION
                )
            )
            // PRECISION
        )
        return dy * (PRECISION - self.fee) // PRECISION

    def exchange(self, i: int, j: int, dx: int, min_dy: int = 0) -> int:
        dy = self.calc_exchange(i, j, dx)
        assert dy >= min_dy, "Slippage"

        self.vxs[i] += dx
        self.vxs[j] -= dy
        self.xs[i] += dx
        self.xs[j] -= dy
        assert self.xs[i] > 0 and self.xs[j] > 0, "Balance zeroed"
        return dy

    def spot_price(self, i: int, j: int) -> int:
        return self.xs[i] * self.ws[j] * PRECISION // (self.xs[j] * self.ws[i])

    def full_balance_in(self, i: int) -> int:
        return self.xs[i] * PRECISION // self.ws[i]
