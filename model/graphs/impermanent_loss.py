from model.graphs.settings import *

GRID_SIZE = 1_000
STEP = 4 * PRECISION // GRID_SIZE
MAX_PRICE_CHANGE = 10


def get_values(proportions: tuple, i: int = 1) -> (list, list):
    initial_amounts = (PRECISION, PRECISION)
    swap = SuperpositionSwap(w)
    swap.add_liquidity(initial_amounts, proportions)

    initial_price = swap.spot_price(i, 1 - i)
    il = [
        swap.full_balance_in(i)
        / (initial_amounts[i] + initial_amounts[1 - i] * initial_price // PRECISION)
    ]
    relative_price_changes = [1]

    for j in range(2):
        il.reverse()
        relative_price_changes.reverse()

        swap = SuperpositionSwap(w)
        swap.add_liquidity(initial_amounts, proportions)
        for _ in range(GRID_SIZE):
            swap.exchange(1 - j, j, STEP)
            new_price = swap.spot_price(i, 1 - i)
            il.append(
                swap.full_balance_in(i)
                / (initial_amounts[i] + initial_amounts[1 - i] * new_price // PRECISION)
            )
            relative_price_changes.append(new_price / initial_price)
            if (
                relative_price_changes[-1] >= MAX_PRICE_CHANGE
                or relative_price_changes[-1] * MAX_PRICE_CHANGE <= 1
            ):
                break

    return il, relative_price_changes


def get_values_balancer(proportions: tuple, i: int = 1) -> (list, list):
    initial_amounts = (PRECISION, PRECISION)
    swap = Balancer(N_COINS, proportions)
    swap.add_liquidity(initial_amounts)

    initial_price = swap.spot_price(i, 1 - i)
    il = [
        swap.full_balance_in(i)
        / (initial_amounts[i] + initial_amounts[1 - i] * initial_price // PRECISION)
    ]
    relative_price_changes = [1]
    for j in range(2):
        il.reverse()
        relative_price_changes.reverse()

        swap = Balancer(N_COINS, proportions)
        swap.add_liquidity(initial_amounts)
        for _ in range(GRID_SIZE):
            swap.exchange(1 - j, j, STEP)
            new_price = swap.spot_price(i, 1 - i)
            il.append(
                swap.full_balance_in(i)
                / (initial_amounts[i] + initial_amounts[1 - i] * new_price // PRECISION)
            )
            relative_price_changes.append(new_price / initial_price)
            if (
                relative_price_changes[-1] >= MAX_PRICE_CHANGE
                or relative_price_changes[-1] * MAX_PRICE_CHANGE <= 1
            ):
                break

    return il, relative_price_changes


if __name__ == "__main__":
    first_price, last_price = 0, 1e8
    for i, proportions in enumerate(PROPORTIONS):
        adjusted_proportions = tuple(
            i * PRECISION // sum(proportions) for i in proportions
        )

        # SuperpositionSwap
        il, relative_price_changes = get_values(adjusted_proportions)
        plt.plot(relative_price_changes, il, color=colors[i], label=f'{proportions}')
        first_price = max(first_price, relative_price_changes[-1])
        last_price = min(last_price, relative_price_changes[0])

        # Balancer
        il, relative_price_changes = get_values_balancer(adjusted_proportions)
        plt.plot(relative_price_changes, il, ":", label=f"{proportions}")
        first_price = max(first_price, relative_price_changes[-1])
        last_price = min(last_price, relative_price_changes[0])

    plt.xlabel("Изменение цены")
    plt.ylabel("Impermanent Loss")
    plt.legend(title="Веса")
    plt.title(f"Impermanent Loss(w - {2 - w})")

    plt.ylim((0.85, 1.01))
    plt.xlim((first_price, last_price))

    plt.show()
    # plt.savefig(f"/Users/romanagureev/Desktop/il_2-w.png")
