import matplotlib.pyplot as plt

from model.Balancer import Balancer
from model.SuperpositionSwap import PRECISION, SuperpositionSwap

# pyplot
colors = [
    "tab:blue",
    "tab:orange",
    "tab:green",
    "tab:red",
    "tab:purple",
    "tab:brown",
    "tab:grey",
    "tab:cyan",
    "tab:olive",
    "tab:pink",
]

# Swap
w = 16
N_COINS = 2

# Parameters
PROPORTIONS = [
    (5, 5),
    (6, 4),
    # (4, 6),
    (7, 3),
    # (3, 7),
    (8, 2),
    # (2, 8),
    # (9, 1),
    # (1, 9),
    (6, 94),
    (94, 6),
]
