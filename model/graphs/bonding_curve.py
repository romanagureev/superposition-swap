from model.graphs.settings import *

GRID_SIZE = 1_000
STEP = 4 * PRECISION // GRID_SIZE


def get_values(proportions: tuple) -> (list, list):
    initial_amounts = (PRECISION, PRECISION)

    x = [initial_amounts[0]]
    y = [initial_amounts[1]]
    for i in range(2):
        x.reverse()
        y.reverse()
        swap = SuperpositionSwap(w)
        swap.add_liquidity(initial_amounts, proportions)

        for j in range(GRID_SIZE):
            swap.exchange(i, 1 - i, STEP)
            x.append(swap.x[0][0] + swap.x[1][0])
            y.append(swap.x[0][1] + swap.x[1][1])
    return x, y


def get_values_balancer(proportions: tuple, i: int = 1) -> (list, list):
    initial_amounts = (PRECISION, PRECISION)

    x = [initial_amounts[0]]
    y = [initial_amounts[1]]
    for i in range(2):
        x.reverse()
        y.reverse()
        swap = Balancer(2, proportions)
        swap.add_liquidity(initial_amounts)

        for j in range(GRID_SIZE):
            swap.exchange(i, 1 - i, STEP)
            x.append(swap.xs[0])
            y.append(swap.xs[1])
    return x, y


if __name__ == "__main__":
    for i, proportions in enumerate(PROPORTIONS):
        adjusted_proportions = tuple(
            i * PRECISION // sum(proportions) for i in proportions
        )

        # SuperpositionSwap
        x, y = get_values(adjusted_proportions)
        plt.plot(x, y, color=colors[i], label=f'{proportions}')

        # Balancer
        x, y = get_values_balancer(adjusted_proportions)
        plt.plot(x, y, ":", color=colors[i])

    plt.xlabel("x")
    plt.ylabel("y")
    plt.legend(title="Weights")
    plt.title(f"Bonding Curve(w = {w})")

    plt.show()
    # plt.savefig(f"/Users/romanagureev/Desktop/bonding_curve_{w}.png")
