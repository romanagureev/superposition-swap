from model.graphs.settings import *

GRID_SIZE = 1_000
STEP = 1 * PRECISION // 1000
MAX_PRICE = 8.0


def get_values(proportions: tuple, i: int = 0) -> (list, list):
    proportions = tuple(i * PRECISION // sum(proportions) for i in proportions)
    prices = [1]
    depths = [0]
    for j in range(2):
        prices.reverse()
        depths.reverse()
        swap = SuperpositionSwap(w)
        swap.add_liquidity((proportions[0], proportions[1]), proportions)
        for k in range(GRID_SIZE):
            swap.exchange(j, 1 - j, STEP)
            depths.append((k + 1) * STEP)
            prices.append(swap.spot_price(i, 1 - i) / 10 ** 18)
    return prices, depths


def get_values_balancer(proportions: tuple, i: int = 0) -> (list, list):
    prices = [1]
    depths = [0]
    for j in range(2):
        prices.reverse()
        depths.reverse()
        swap = Balancer(2, proportions)
        swap.add_liquidity((proportions[0], proportions[1]))
        for k in range(GRID_SIZE):
            dy = swap.exchange(j, 1 - j, STEP)
            depths.append((k + 1) * STEP)
            prices.append(swap.spot_price(i, 1 - i) / 10 ** 18)
            if prices[-1] >= MAX_PRICE or prices[-1] * MAX_PRICE <= 1:
                break
    return prices, depths


if __name__ == "__main__":
    lowest_price, largest_price = 0, 1e8
    for i, proportions in enumerate(PROPORTIONS):
        adjusted_proportions = tuple(
            i * PRECISION // sum(proportions) for i in proportions
        )

        # SuperpositionSwap
        prices, depths = get_values(adjusted_proportions)
        plt.plot(prices, depths, color=colors[i], label=f'{proportions}')
        lowest_price = max(lowest_price, min(prices))
        largest_price = min(largest_price, max(prices))

        # Balancer
        prices, depths = get_values_balancer(adjusted_proportions)
        plt.plot(prices, depths, ":", color=colors[i], label=f"{proportions}")
        lowest_price = max(lowest_price, min(prices))
        largest_price = min(largest_price, max(prices))

    plt.xlabel("Цена")
    plt.ylabel("Глубина")
    plt.legend(title="Веса")
    plt.title(f"Глубина")

    plt.xscale("log")
    plt.xlim(
        (max(lowest_price, 1 / largest_price), min(1 / lowest_price, largest_price))
    )

    plt.show()
    # plt.savefig(f"/Users/romanagureev/Desktop/depth.png")
