# @version 0.3.3
"""
@title ProportionedSwap
@author Roman Agureev
@license MIT
"""

from vyper.interfaces import ERC20
from vyper.interfaces import ERC165

interface wETH:
    def deposit(): payable
    def withdraw(_amount: uint256): nonpayable

interface ERC1271:
     def isValidSignature(_hash: bytes32, _signature: Bytes[65]) -> bytes32: view

# ERC1155
event TransferSingle:
    _operator: indexed(address)
    _from: indexed(address)
    _to: indexed(address)
    _id: uint256
    _value: uint256

event TransferBatch:
    _operator: indexed(address)
    _from: indexed(address)
    _to: indexed(address)
    _ids: DynArray[uint256, N_POOLS]
    _values: DynArray[uint256, N_POOLS]

event ApprovalForAll:
    _owner: indexed(address)
    _operator: indexed(address)
    _approved: bool

interface IERC1155Receiver:
    def onERC1155Received(
       operator: address,
       sender: address,
       id: uint256,
       amount: uint256,
       data: Bytes[CALLBACK_NUMBYTES],
   ) -> bytes32: payable
    def onERC1155BatchReceived(
        operator: address,
        sender: address,
        ids: DynArray[uint256, BATCH_SIZE],
        amounts: DynArray[uint256, BATCH_SIZE],
        data: Bytes[CALLBACK_NUMBYTES],
    ) -> bytes4: payable


# ERC20
event Approval:
    sender: indexed(address)
    spender: indexed(address)
    id: uint256
    value: uint256


# Swap
event AddLiquidity:
    provider: indexed(address)
    amounts: uint256[N_COINS]
    lp_amounts: uint256[N_POOLS]

event RemoveLiquidity:
    provider: indexed(address)
    amounts: uint256[N_COINS]
    lp_amounts: uint256[N_POOLS]

event Exchange:
    trader: indexed(address)
    from_id: uint256
    from_amount: uint256
    to_id: uint256
    to_amount: uint256

# maximum items in a batch call. Set to 128, to be determined what the practical limits are.
BATCH_SIZE: constant(uint256) = 2
CALLBACK_NUMBYTES: constant(uint256) = 4096

ERC165_INTERFACE_id: constant(bytes4)  = 0x01ffc9a7
ERC1155_INTERFACE_id: constant(bytes4) = 0xd9b67a26

# keccak256("isValidSignature(bytes32,bytes)")[:4] << 224
ERC1271_MAGIC_VAL: constant(bytes32) = 0x1626ba7e00000000000000000000000000000000000000000000000000000000
EIP712_TYPEHASH: constant(bytes32) = keccak256("EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)")
PERMIT_TYPEHASH: constant(bytes32) = keccak256("Permit(address owner,address spender,uint256 value,uint256 nonce,uint256 deadline)")
domain_separator: bytes32

# ERC1155
name: public(String[64])
symbol: public(String[32])
# Mapping from token ID to account balances
balanceOf: public(HashMap[address, uint256[N_POOLS]])
# Mapping from account to operator approvals
isApprovedForAll: public(HashMap[address, HashMap[address, bool]])
totalSupply: public(uint256[N_POOLS])

nonces: public(HashMap[address, uint256[N_POOLS]])
allowance: public(HashMap[address, HashMap[address, uint256[N_POOLS]]])

# Swap
W: constant(uint256) = 16
PRECISION: constant(uint256) = 10 ** 18
N_COINS: constant(uint256) = 2
N_POOLS: constant(uint256) = N_COINS
VERSION: constant(String[8]) = "v0.0.0"

FACTORY: immutable(address)
WETH: immutable(address)

coins: public(address[N_COINS])
xs: public(uint256[N_POOLS][N_COINS])
rates: uint256[N_COINS]

fee: public(uint256)
ADMIN_FEE: constant(uint256) = 2
MIN_FEE: constant(uint256) = PRECISION / 100 / 100 / 10  # 0.1 bps
MAX_FEE: constant(uint256) = PRECISION / 20  # 5%
DEFAULT_FEE: constant(uint256) = 3 * PRECISION / 1000  # 0.3%

weights: public(uint256[N_COINS])


@external
def __init__(_factory: address, _weth: address):
    """
    @notice Constructor of implementation contract
    @param _factory Address of the factory
    @param _weth Address of wrapped ETH
    """
    FACTORY = _factory
    WETH = _weth


@payable
@external
def __default__():
    assert msg.sender.is_contract


@external
def initialize(_name: String[64], _symbol: String[32], _coins: address[N_COINS], _rates: uint256[N_COINS]):
    """
    @notice Constructor for WeightedSwap
    @dev Should be executed from factory
    @param _name Name of the pool
    @param _coins Coins for swap
    @param _rates Rates for coins
    """
    assert msg.sender == FACTORY, "Deploy through factory"

    self.coins = _coins
    self.rates = _rates
    self.fee = DEFAULT_FEE

    self.name = _name
    self.symbol = _symbol

    self.domain_separator = keccak256(
        _abi_encode(EIP712_TYPEHASH, keccak256(_name), keccak256(VERSION), chain.id, self)
    )

    # fire a transfer event so block explorers identify the contract as an ERC20
    #self._mint_many(self, empty(uint256[N_COINS]))


### ERC1155 Functionality ###

@pure
@external
def supportsInterface(_interface_id: bytes4) -> bool:
    """
    @dev Returns True if the interface is supported
    @param _interface_id bytes4 interface identifier
    """
    return _interface_id in [
        ERC165_INTERFACE_id,
        ERC1155_INTERFACE_id,
    ]

@pure
@external
def decimals(_id: uint256 = 0) -> uint256:
    """
    @notice Get the number of decimals for this token
    @dev Implemented as a pure method to reduce gas costs
    @return uint256 decimal places
    """
    return 18


@external
@view
def balanceOfBatch(_accounts: DynArray[address, BATCH_SIZE], _ids: DynArray[uint256, BATCH_SIZE]) -> DynArray[uint256,BATCH_SIZE]:  # uint256[BATCH_SIZE]:
    """
    @dev check the balance for an array of specific IDs and addresses
    @dev will return an array of balances
    @dev Can also be used to check ownership of an ID
    @param _accounts a dynamic array of the addresses to check the balance for
    @param _ids a dynamic array of the token IDs to check the balance
    """
    assert len(_accounts) == len(_ids), "ERC1155: accounts and ids length mismatch"
    batchBalances: DynArray[uint256, BATCH_SIZE] = []
    j: uint256 = 0
    for id in _ids:
        batchBalances.append(self.balanceOf[_accounts[j]][id])
        j += 1
    return batchBalances


@external
def setApprovalForAll(_owner: address, _operator: address, _approved: bool):
    """
    @dev set an operator for a certain NFT owner address
    @param _owner the owner address
    @param _operator the operator address
    @param _approved value to set
    """
    assert _owner == msg.sender, "You can only set operators for your own account"
    assert _owner != _operator, "ERC1155: setting approval status for self"
    self.isApprovedForAll[_owner][_operator] = _approved
    log ApprovalForAll(_owner, _operator, _approved)


@internal
def _transfer(_sender: address, _receiver: address, _id: uint256, _amount: uint256):
    assert _receiver != ZERO_ADDRESS, "ERC1155: transfer to the zero address"
    assert _sender != _receiver
    allowance: uint256 = self.allowance[_sender][msg.sender][_id]
    if allowance > _amount:
        self.allowance[_sender][msg.sender][_id] = allowance - _amount
    else:
        assert _sender == msg.sender or self.isApprovedForAll[_sender][msg.sender], "Caller is neither owner nor approved operator for this ID"
    assert self.balanceOf[_sender][_id] > 0, "ZERO balance"

    operator: address = msg.sender
    self.balanceOf[_sender][_id] -= _amount
    self.balanceOf[_receiver][_id] += _amount
    log TransferSingle(operator, _sender, _receiver, _id, _amount)


@external
def transferFrom(_sender: address, _receiver: address, _id: uint256, _amount: uint256):
    """
    @dev transfer token from one address to another.
    @param _sender the sending account (current owner)
    @param _receiver the receiving account
    @param _id the token id that will be sent
    @param _amount the amount of tokens for the specified id
    """
    self._transfer(_sender, _receiver, _id, _amount)


@external
def safeTransferFrom(_sender: address, _receiver: address, _id: uint256, _amount: uint256, _bytes: bytes32):
    """
    @dev transfer token from one address to another.
    @param _sender the sending account (current owner)
    @param _receiver the receiving account
    @param _id the token id that will be sent
    @param _amount the amount of tokens for the specified id
    """
    self._transfer(_sender, _receiver, _id, _amount)
    # TODO on received


@internal
def _batch_transfer_from(_sender: address, _receiver: address, _ids: DynArray[uint256, BATCH_SIZE], _amounts: DynArray[uint256, BATCH_SIZE]):
    assert _receiver != ZERO_ADDRESS, "ERC1155: transfer to the zero address"
    assert _sender != _receiver
    # TODO allowances
    assert _sender == msg.sender or self.isApprovedForAll[_sender][msg.sender], "Caller is neither owner nor approved operator for this ID"
    assert len(_ids) == len(_amounts), "ERC1155: ids and amounts length mismatch"
    operator: address = msg.sender
    for i in range(BATCH_SIZE):
        if i >= len(_ids):
            break
        id: uint256 = _ids[i]
        amount: uint256 = _amounts[i]
        self.balanceOf[_sender][id] -= amount
        self.balanceOf[_receiver][id] += amount

    log TransferBatch(operator, _sender, _receiver, _ids, _amounts)


@external
def batchTransferFrom(_sender: address, _receiver: address, _ids: DynArray[uint256, BATCH_SIZE], _amounts: DynArray[uint256, BATCH_SIZE], _bytes: bytes32):
    """
    @dev transfer tokens from one address to another.
    @param _sender the sending account
    @param _receiver the receiving account
    @param _ids a dynamic array of the token ids that will be sent
    @param _amounts a dynamic array of the amounts for the specified list of ids.
    """
    self._batch_transfer_from(_sender, _receiver, _ids, _amounts)


@external
def safeBatchTransferFrom(_sender: address, _receiver: address, _ids: DynArray[uint256, BATCH_SIZE], _amounts: DynArray[uint256, BATCH_SIZE], _bytes: bytes32):
    """
    @dev transfer tokens from one address to another.
    @param _sender the sending account
    @param _receiver the receiving account
    @param _ids a dynamic array of the token ids that will be sent
    @param _amounts a dynamic array of the amounts for the specified list of ids.
    """
    self._batch_transfer_from(_sender, _receiver, _ids, _amounts)
    # TODO on received


@external
def approve(_spender: address, _id: uint256, _value: uint256) -> bool:
    """
    @notice Approve the passed address to transfer the specified amount of
            tokens on behalf of msg.sender
    @dev Beware that changing an allowance via this method brings the risk that
         someone may use both the old and new allowance by unfortunate transaction
         ordering: https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
    @param _spender The address which will transfer the funds
    @param _id Token ID
    @param _value The amount of tokens that may be transferred
    @return bool success
    """
    self.allowance[msg.sender][_spender][_id] = _value

    log Approval(msg.sender, _spender, _id, _value)
    return True


@external
def permit(
    _owner: address,
    _spender: address,
    _id: uint256,
    _value: uint256,
    _deadline: uint256,
    _v: uint8,
    _r: bytes32,
    _s: bytes32
) -> bool:
    """
    @notice Approves spender by owner's signature to expend owner's tokens.
        See https://eips.ethereum.org/EIPS/eip-2612.
    @dev Inspired by https://github.com/yearn/yearn-vaults/blob/main/contracts/Vault.vy#L753-L793
    @dev Supports smart contract wallets which implement ERC1271
        https://eips.ethereum.org/EIPS/eip-1271
    @param _owner The address which is a source of funds and has signed the Permit.
    @param _spender The address which is allowed to spend the funds.
    @param _id Token ID
    @param _value The amount of tokens to be spent.
    @param _deadline The timestamp after which the Permit is no longer valid.
    @param _v The bytes[64] of the valid secp256k1 signature of permit by owner
    @param _r The bytes[0:32] of the valid secp256k1 signature of permit by owner
    @param _s The bytes[32:64] of the valid secp256k1 signature of permit by owner
    @return True, if transaction completes successfully
    """
    assert _owner != ZERO_ADDRESS
    assert block.timestamp <= _deadline

    nonce: uint256 = self.nonces[_owner][_id]
    digest: bytes32 = keccak256(
        concat(
            b"\x19\x01",
            self.domain_separator,
            keccak256(_abi_encode(PERMIT_TYPEHASH, _owner, _spender, _value, nonce, _deadline))
        )
    )

    if _owner.is_contract:
        sig: Bytes[65] = concat(_abi_encode(_r, _s), slice(convert(_v, bytes32), 31, 1))
        # reentrancy not a concern since this is a staticcall
        assert ERC1271(_owner).isValidSignature(digest, sig) == ERC1271_MAGIC_VAL
    else:
        assert ecrecover(digest, convert(_v, uint256), convert(_r, uint256), convert(_s, uint256)) == _owner

    self.allowance[_owner][_spender][_id] = _value
    self.nonces[_owner][_id] = nonce + 1

    log Approval(_owner, _spender, _id, _value)
    return True


@internal
def _mint(_to: address, _id: uint256, _amount: uint256):
    self.balanceOf[_to][_id] += _amount
    log TransferSingle(msg.sender, ZERO_ADDRESS, _to, _id, _amount)


@internal
def _mint_many(_to: address, _amounts: uint256[N_POOLS]):
    amounts: DynArray[uint256, N_POOLS] = []
    for i in range(N_COINS):
        self.balanceOf[_to][i] += _amounts[i]
        amounts.append(_amounts[i])
    log TransferBatch(msg.sender, ZERO_ADDRESS, _to, [0, 1], amounts)


@internal
def _burn_many(_amounts: uint256[N_POOLS]):
    amounts: DynArray[uint256, N_POOLS] = []
    for i in range(N_POOLS):
        self.balanceOf[msg.sender][i] -= _amounts[i]
        amounts.append(_amounts[i])
    log TransferBatch(msg.sender, msg.sender, ZERO_ADDRESS, [0, 1], amounts)


### Math Functions ###


@pure
@internal
def _bpowi(_a: uint256, _n: uint256) -> uint256:
    z: uint256 = _a
    if _n % 2 == 0:
        z = PRECISION

    n: uint256 = _n
    a: uint256 = _a
    for i in range(256):
        n /= 2
        a *= a

        if n % 2 != 0:
            z = z * a
    return z


@pure
@internal
def _bpow_approx(_base: uint256, _exp: uint256) -> uint256:
    a: uint256 = _exp
    x: uint256 = 0
    xneg: bool = True
    if _base > PRECISION:
        x = _base - PRECISION
        xneg = False
    else:
        x = PRECISION - _base
    term: uint256 = PRECISION
    sum: uint256 = term
    negative: bool = False

    # term(k) = numer / denom
    #         = (product(a - i - 1, i=1-->k) * x^k) / (k!)
    # each iteration, multiply previous term by (a-(k-1)) * x / k
    # continue until term is less than precision
    for i in range(1, 256):
        if term < 10 ** 6:  # precision of calculations
            break
        big_k: uint256 = i * PRECISION
        tmp: uint256 = big_k - PRECISION
        c: uint256 = 0
        cneg: bool = True
        if a > tmp:
            c = a - tmp
            cneg = False
        else:
            c = tmp - a
        term = term * c * x
        term = term / big_k
        if term == 0:
            break

        if xneg:
            negative = not negative
        if cneg:
            negative = not negative
        if negative:
            sum -= term
        else:
            sum += term
    return sum


# Compute b^(e.w) by splitting it into (b^e)*(b^0.w).
# Use `bpowi` for `b^e` and `bpowK` for k iterations
# of approximation of b^0.w
@view
@internal
def _bpow(_base: uint256, _exp: uint256) -> uint256:
    # assert _base >= MIN_BPOW_BASE, "base too low"
    # assert _base <= MAX_BPOW_BASE, "base too high"

    whole: uint256 = _exp / PRECISION
    remain: uint256 = _exp - whole * PRECISION

    whole_pow: uint256 = self._bpowi(_base, whole)

    if remain == 0:
        return whole_pow

    partial: uint256 = self._bpow_approx(_base, remain)
    return whole_pow * partial / PRECISION


@pure
@internal
def _weight(_i: uint256, _j: uint256) -> uint256:
    if _i == _j:
        return W
    else:
        return 1


@view
@internal
def _norm_weight(_i: uint256, _j: uint256) -> uint256:
    return self._weight(_i, _j) * PRECISION / (1 + W)


@view
@internal
def _level(_i: uint256) -> uint256:
    level: uint256 = PRECISION
    for i in range(N_COINS):
        level *= self._bpow(self.xs[_i][i], self._norm_weight(_i, i))
        level /= PRECISION
    return level


### SuperpositionSwap Functionality ###


@view
@external
def get_virtual_price(_i: uint256) -> uint256:
    return self._level(_i) * PRECISION / self.totalSupply[_i]


@pure
@external
def factory() -> address:
    return FACTORY


@payable
@internal
def _receive_coin(_coin: address, _amount: uint256, _use_eth: bool):
    if _coin == WETH:
        if _use_eth:
            assert msg.value == _amount, "Incorrect ETH amount"
            wETH(WETH).deposit(value=_amount)
            return
        else:
            assert msg.value == 0, "Incorrect ETH amount"
    ERC20(_coin).transferFrom(msg.sender, self, _amount)


@internal
def _transfer_coin(_coin: address, _amount: uint256, _use_eth: bool, _receiver: address):
    if _use_eth and _coin == WETH:
        wETH(WETH).withdraw(_amount)
        raw_call(_receiver, b"", value=_amount)
    else:
        ERC20(_coin).transfer(_receiver, _amount)


@payable
@external
@nonreentrant("lock")
def add_liquidity(_amounts: uint256[N_COINS], _min_lp: uint256[N_POOLS], _use_eth: bool = False, _receiver: address = msg.sender) -> uint256[N_POOLS]:
    lp: uint256[N_POOLS] = empty(uint256[N_POOLS])
    if self.totalSupply[0] == 0:  # Initial provide
        self.xs[1][0] = _amounts[0] / (1 + W)
        self.xs[0][0] = _amounts[0] - self.xs[1][0]

        self.xs[0][1] = _amounts[1] / (1 + W)
        self.xs[1][1] = _amounts[1] - self.xs[0][1]

        for i in range(N_POOLS):
            lp[i] = self._level(i)
    else:
        a0: uint256 = (_amounts[1] * self.xs[0][0] * self.xs[1][0] - _amounts[0] * self.xs[0][0] * self.xs[1][1]) / (self.xs[0][1] * self.xs[1][0] - self.xs[0][0] * self.xs[1][1])
        a1: uint256 = a0 * self.xs[0][1] / self.xs[0][0]

        initial_levels: uint256[N_POOLS] = empty(uint256[N_POOLS])
        for i in range(N_POOLS):
            initial_levels[i] = self._level(i)

        self.xs[0][0] += a0
        self.xs[0][1] += a1
        self.xs[1][0] += _amounts[0] - a0
        self.xs[1][1] += _amounts[1] - a1

        new_levels: uint256[N_POOLS] = empty(uint256[N_POOLS])
        for i in range(N_POOLS):
            new_levels[i] = self._level(i)
        for i in range(N_POOLS):
            lp[i] = self.totalSupply[i] * new_levels[i] / initial_levels[i] - self.totalSupply[i]
    for i in range(N_COINS):
        self._receive_coin(self.coins[i], _amounts[i], _use_eth)
    self._mint_many(_receiver, lp)
    for i in range(N_POOLS):
        assert lp[i] >= _min_lp[i], "Slippage screwed you"
        self.totalSupply[i] += lp[i]
    return lp


@external
@nonreentrant("lock")
def remove_liquidity(_lp: uint256[N_POOLS], _min_amounts: uint256[N_COINS], _use_eth: bool = False, _receiver: address = msg.sender) -> uint256[N_COINS]:
    amounts: uint256[N_COINS] = empty(uint256[N_COINS])
    for i in range(N_POOLS):
        supply: uint256 = self.totalSupply[i]
        self.totalSupply[i] = supply - _lp[i]
        for j in range(N_COINS):
            amount: uint256 = self.xs[i][j] * _lp[i] / supply
            amounts[j] += amount
            self.xs[i][j] -= amount
    self._burn_many(_lp)
    rates: uint256[N_COINS] = self.rates
    for i in range(N_COINS):
        amounts[i] /= rates[i]
        assert amounts[i] >= _min_amounts[i], "Slippage screwed you"
        self._transfer_coin(self.coins[i], amounts[i], _use_eth, _receiver)
    return amounts


@external
@nonreentrant("lock")
def remove_liquidity_one_coin(_lp: uint256[N_POOLS], _i: uint256, _min_amount: uint256, _use_eth: bool = False, _receiver: address = msg.sender) -> uint256:
    return 0


@view
@internal
def _calc_f(_k: uint256, _i: uint256, _dx: uint256) -> uint256:
    f: uint256 = (self.xs[_k][_i] + _dx) * self._weight(_k, 1 - _i) * PRECISION / (self.xs[_k][1 - _i] * self._weight(_k, _i))
    ratio: uint256 = (self.xs[_k][_i] * PRECISION + _dx * (PRECISION - self.fee)) / self.xs[_k][_i]
    w_ratio: uint256 = self._weight(_k, _i) * PRECISION / self._weight(_k, 1 - _i)
    f *= self._bpow(ratio, w_ratio)
    return f


@view
@internal
def _calc_dy(_k: uint256, _from: uint256, _dx: uint256) -> uint256:
    ratio: uint256 = self.xs[_k][_from] * PRECISION / (self.xs[_k][_from] + _dx * (PRECISION - self.fee) / PRECISION)
    w_ratio: uint256 = self._weight(_k, _from) * PRECISION / self._weight(_k, 1 - _from)
    return self.xs[_k][1 - _from] * (PRECISION - self._bpow(ratio, w_ratio)) / PRECISION


@view
@internal
def _calc_exchange(_from: uint256, _to: uint256, _dx: uint256) -> uint256[N_COINS * N_POOLS]:
    f0: uint256 = self._calc_f(0, _from, 0)
    f1: uint256 = self._calc_f(1, _from, _dx)
    left: uint256 = 0
    if f0 > f1:
        left = _dx
    right: uint256 = _dx - left
    for i in range(256):
        if unsafe_sub(unsafe_add(right, 1), left) <= 1:
            break
        mid: uint256 = (left + right) / 2
        f0 = self._calc_f(0, _from, mid)
        f1 = self._calc_f(1, _from, _dx - mid)
        if f0 < f1:
            left = mid
        else:
            right = mid

    d: uint256[N_COINS * N_POOLS] = empty(uint256[N_COINS * N_POOLS])
    d[0] = left
    d[1] = _dx - left
    for i in range(N_POOLS):
        d[N_POOLS + i] = self._calc_dy(i, _from, d[i])
    return d


@external
def calc_exchange(_from: uint256, _to: uint256, _dx: uint256) -> uint256:
    d: uint256[N_COINS * N_POOLS] = self._calc_exchange(_from, _to, _dx * self.rates[_from])
    return (d[N_POOLS] + d[N_POOLS + 1]) / self.rates[_to]


@payable
@external
@nonreentrant("lock")
def exchange(_from: uint256, _to: uint256, _dx: uint256, _min_dy: uint256, _use_eth: bool = False, _receiver: address = msg.sender) -> uint256:
    d: uint256[N_COINS * N_POOLS] = self._calc_exchange(_from, _to, _dx * self.rates[_from])
    dy: uint256 = (d[N_POOLS] + d[N_POOLS + 1]) / self.rates[_to]
    assert dy >= _min_dy, "Slippage screwed you"

    for i in range(N_POOLS):
        self.xs[i][0] += d[i]
        self.xs[i][1] -= d[N_POOLS + i]

    self._receive_coin(self.coins[_from], _dx, _use_eth)
    self._transfer_coin(self.coins[_to], dy, _use_eth, _receiver)
    return dy


@external
def get_admin_fees(_coin: address = ZERO_ADDRESS) -> DynArray[uint256, N_COINS]:
    fees: DynArray[uint256, N_COINS] = []
    if _coin == ZERO_ADDRESS:
        for i in range(N_COINS):
            coin: address = self.coins[i]
            amount: uint256 = ERC20(coin).balanceOf(self) - (self.xs[0][i] + self.xs[1][i]) / self.rates[i]
            ERC20(coin).transfer(FACTORY, amount)
            fees.append(amount)
    else:
        # Donating coins accidentally transferred to this contract
        assert _coin not in self.coins
        amount: uint256 = ERC20(_coin).balanceOf(self)
        ERC20(_coin).transfer(FACTORY, amount)
        fees.append(amount)
    return fees


@external
def set_new_parameters(_new_fee: uint256):
    """
    @dev Callable only by factory
    @param _new_fee Fee to set
    """
    assert msg.sender == FACTORY, "only factory"
    assert _new_fee >= MIN_FEE, "Too low fee"
    assert _new_fee <= MAX_FEE, "Too big fee"
    self.fee = _new_fee


@pure
@external
def version() -> String[8]:
    """
    @notice Get the version of this token contract
    """
    return VERSION
