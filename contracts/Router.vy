# @version 0.3.3
"""
@title Router for WeightedSwaps
@author Roman Agureev
@license MIT
"""

from vyper.interfaces import ERC20

interface Pool:
    def exchange(_from: uint256, _to: uint256, _dx: uint256, _min_dy: uint256,
         _use_eth: bool = False, _receiver: address = msg.sender) -> uint256: payable


interface Factory:
    def get_market(_token0: address, _token1: address) -> address: view


FACTORY: immutable(Factory)
approved: HashMap[address, bool]


@external
def __init__(_factory: address):
    FACTORY = Factory(_factory)


@external
def exchange(_tokens: DynArray[address, 8], _dx: uint256, _min_dy: uint256, _receiver: address) -> uint256:
    assert len(_tokens) > 1
    ERC20(_tokens[0]).transferFrom(msg.sender, self, _dx)
    dx: uint256 = _dx
    for i in range(1, 8):
        if _tokens[i] == ZERO_ADDRESS:
            break
        pool: address = FACTORY.get_market(_tokens[i - 1], _tokens[i])
        if not self.approved[pool]:
            ERC20(_tokens[i - 1]).approve(pool, MAX_UINT256)
            ERC20(_tokens[i]).approve(pool, MAX_UINT256)
            self.approved[pool] = True
        dx = Pool(pool).exchange(0, 1, dx, 0)
    assert dx >= _min_dy
    ERC20(_tokens[len(_tokens) - 1]).transfer(_receiver, dx)
    return dx
