# @version 0.3.3
"""
@title Factory for WeightedSwaps
@author Roman Agureev
@license MIT
"""


interface ERC20:
    def decimals() -> uint256: view


event PoolDeployed:
    pool: address
    coins: address[N_COINS]
    deployer: address


interface Pool:
    def exchange(_from: uint256, _to: uint256, _dx: uint256, _min_dy: uint256,
         _use_eth: bool = False, _receiver: address = msg.sender) -> uint256: payable
    def initialize(_name: String[64], _symbol: String[32], _coins: address[N_COINS], _rates: uint256[N_COINS]): nonpayable


N_COINS: constant(uint256) = 2

base_implementation: public(address)
meta_implementation: public(address)

pool_list: public(address[4294967296])   # master list of pools
pool_count: public(uint256)              # actual length of pool_list

# mapping of coins -> pools for trading
# a mapping key is generated for each pair of addresses via
# `bitwise_xor(convert(a, uint256), convert(b, uint256))`
markets: HashMap[uint256, address[4294967296]]
market_counts: HashMap[uint256, uint256]


# Priorities for burning coins
priority_of: public(HashMap[address, uint256])

# Ownership
admin: public(address)
parameter_admin: public(address)

future_admin: public(address)
future_parameter_admin: public(address)


@external
def __init__():
    self.admin = msg.sender
    self.parameter_admin = msg.sender


@view
@internal
def _get_market(_token0: address, _token1: address) -> address:
    return self.markets[convert(_token0, uint256)][convert(_token1, uint256)]


@view
@external
def get_market(_token0: address, _token1: address) -> address:
    return self._get_market(_token0, _token1)


@external
def deploy_base(
    _name: String[32],
    _symbol: String[10],
    _coins: address[2],
) -> address:
    """
    @notice Deploy a new pool
    @param _name Name of the new plain pool
    @param _symbol Symbol for the new plain pool - will be concatenated with factory symbol
    Other parameters need some description
    @return Address of the deployed pool
    """
    assert _coins[0] != _coins[1], "Duplicate coins"

    decimals: int128[2] = empty(int128[2])
    for i in range(2):
        d: int128 = convert(ERC20(_coins[i]).decimals(), int128)
        assert d < 19, "Max 18 decimals for coins"
        decimals[i] = d
    rates: uint256[N_COINS] = [shift(1, 18 - decimals[0]), shift(1, 18 - decimals[1])]

    name: String[64] = concat("SuperpositionSwap: ", _name)
    symbol: String[32] = concat(_symbol, "-f")

    pool: address = create_forwarder_to(self.base_implementation)
    Pool(pool).initialize(name, symbol, _coins, rates)

    length: uint256 = self.pool_count
    self.pool_list[length] = pool
    self.pool_count = length + 1

    key: uint256 = bitwise_xor(convert(_coins[0], uint256), convert(_coins[1], uint256))
    length = self.market_counts[key]
    self.markets[key][length] = pool
    self.market_counts[key] = length + 1

    log PoolDeployed(pool, _coins, msg.sender)
    return pool


@external
def deploy_meta(
    _name: String[32],
    _symbol: String[10],
    _coins: address[2],
) -> address:
    """
    @notice Deploy a new pool
    @param _name Name of the new plain pool
    @param _symbol Symbol for the new plain pool - will be concatenated with factory symbol
    Other parameters need some description
    @return Address of the deployed pool
    """
    assert _coins[0] != _coins[1], "Duplicate coins"
    # assert _coins[1] TODO check lp token

    decimals: int128[2] = empty(int128[2])
    for i in range(2):
        d: int128 = convert(ERC20(_coins[i]).decimals(), int128)
        assert d < 19, "Max 18 decimals for coins"
        decimals[i] = d
    rates: uint256[N_COINS] = [shift(1, 18 - decimals[0]), shift(1, 18 - decimals[1])]

    name: String[64] = concat("SuperpositionSwapMeta: ", _name)
    symbol: String[32] = concat(_symbol, "-f")

    pool: address = create_forwarder_to(self.meta_implementation)
    Pool(pool).initialize(name, symbol, _coins, rates)

    length: uint256 = self.pool_count
    self.pool_list[length] = pool
    self.pool_count = length + 1

    key: uint256 = bitwise_xor(convert(_coins[0], uint256), convert(_coins[1], uint256))
    length = self.market_counts[key]
    self.markets[key][length] = pool
    self.market_counts[key] = length + 1

    log PoolDeployed(pool, _coins, msg.sender)
    return pool


# Burning admin fees


@external
def set_priority(_coin: address, _priority: uint256):
    assert msg.sender == self.parameter_admin
    self.priority_of[_coin] = _priority


@external
def burn(_from: address, _to: address) -> uint256:
    assert self.priority_of[_from] <= self.priority_of[_to]
    pool: Pool = Pool(self._get_market(_from, _to))
    # Exchange
    return 0


# Admin functions


@external
def set_implementation(_base: address, _meta: address):
    assert msg.sender == self.admin, "Only admin"
    self.base_implementation = _base
    self.meta_implementation = _meta


@external
def commit_new_admins(_admin: address, _parameter_admin: address):
    assert msg.sender == self.admin, "Only admin"
    self.future_admin = _admin
    self.parameter_admin = _parameter_admin


@external
def apply_new_admins():
    assert msg.sender == self.future_admin, "Only future admin"
    self.admin = msg.sender
    self.parameter_admin = self.future_parameter_admin
